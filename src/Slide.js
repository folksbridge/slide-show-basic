import React from 'react';

const Slide = ({ image, show, slideDir }) => {

    const showFlag = show===1?'inline-block':'none';
    const dir = slideDir;

    const styles = { 
        display: `${showFlag}`,
        animation: `fade-in 2s`,
    }

    return <div className="slide" style={styles}>
        <img class="img" style= {{animation: `slide-${dir} 2s`}} src = {image} alt="img"></img>
    </div>
  }

export default Slide;